package com.example.hclTest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bank")
public class Bank {
	
	@Column(name="Bankname")
	String bankName;
	@Id
	String iban;

}
