package com.example.hclTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HclTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(HclTestApplication.class, args);
	}

}
