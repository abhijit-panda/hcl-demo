package com.example.hclTest.service;

import java.util.List;

import com.example.hclTest.entity.AccountDetails;

public interface FavBankAccountService {
	
	public List<AccountDetails> getAccountDetails(int id);

	public String addAccount(AccountDetails details);

}
