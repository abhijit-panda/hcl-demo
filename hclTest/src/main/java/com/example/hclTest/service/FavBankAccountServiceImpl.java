package com.example.hclTest.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.hclTest.entity.AccountDetails;
import com.example.hclTest.repository.FavBankAccountRepository;

@Service
public class FavBankAccountServiceImpl implements FavBankAccountService {
	
	@Autowired
	FavBankAccountRepository favBankrepository;

	@Override
	public List<AccountDetails> getAccountDetails(int id) {
		// TODO Auto-generated method stub
		List<AccountDetails> allAccounts=favBankrepository.findAll();
		List<AccountDetails> favAccounts=allAccounts.stream().filter((a)->a.getCustomerid() == id).collect(Collectors.toList());
		return favAccounts;
	}

	@Override
	public String addAccount(AccountDetails details) {
		String message="failed";
		// TODO Auto-generated method stub
		try {
			favBankrepository.save(details);
			message="Success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return message;
	}
	

}
