package com.example.hclTest.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.hclTest.entity.AccountDetails;
import com.example.hclTest.entity.Customer;
import com.example.hclTest.service.FavBankAccountService;

@RestController
@RequestMapping(path = "/favbank")
public class FavBankAccountController {
	
	@Autowired
	FavBankAccountService favBankAccountService;
	
	
	@RequestMapping(value="/accounts/{id}", method=RequestMethod.GET, produces="application/json")
	public List<AccountDetails> getFavAccounts(@PathVariable("id") int id) {
		
		List<AccountDetails> accountDetails;
		try {
			accountDetails = favBankAccountService.getAccountDetails(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			accountDetails=null;

			e.printStackTrace();
		}
		return accountDetails;
		
	}
	
	@RequestMapping(value="/addAccounts", method=RequestMethod.GET, produces="application/json")
	public String addFavAccounts(AccountDetails details) {
		
		String message=favBankAccountService.addAccount(details);
		return message;
		
	}

}
