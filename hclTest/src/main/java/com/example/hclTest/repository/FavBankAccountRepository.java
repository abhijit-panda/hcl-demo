package com.example.hclTest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.hclTest.entity.AccountDetails;

@Repository
public interface FavBankAccountRepository extends JpaRepository<AccountDetails, String> {
	

}
